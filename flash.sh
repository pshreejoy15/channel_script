#! /bin/sh
#
# Copyright (C) 2019 Shreejoy Dash.
#
# Licensed under the Dash Public License, Version 1.0 (the "License");
# you may not use this file except in compliance with the License.
#
# Channel auto-posting script.


. "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/stacks/telegram
  Date="$(date +%Y/%m/%d)"

function get_commit() {
      export ID=$TRAVIS_COMMIT
	  git diff-tree --no-commit-id --name-only -r $TRAVIS_COMMIT > commit.txt
      cat commit.txt | cut -d "/" -f1
}

function get_device() {
      mkdir python
      cd python
	  wget https://raw.githubusercontent.com/shreejoy/OTA/master/test.py
	  wget https://gitlab.com/pshreejoy15/channel_script/raw/master/jsonformatter.py
	  cd ..
      for i in $(jq -r ".[] | .codename" devices.json)
  do
      if grep $i commit.txt; then
      echo -e " ";
      echo -e "There is a new update for $i ";
      echo -e " ";
      echo -e "Getting build information from remote source !!";
      echo -e " ";
      rm -rf build.json $i* info.json
	  cd python
      wget https://raw.githubusercontent.com/shreejoy/OTA/master/info.json 
      wget https://raw.githubusercontent.com/PixysOS-Devices/official_devices/master/$i/build.json
	  cd ..
      cat devices.json | jq --arg i "$i" '.[] | select(.codename==$i)' > python/$i.json
	  export DEVICE=$i
      else
      echo -e "No new update for $i"
      fi
done	
}  
	  
function get_json {
    cd python
	mv test.py tg_post.py
    head -n -1 info.json > info1.json
    rm -rf info.json
    mv info1.json info.json
    echo "," >> info.json
    head -n -3 build.json > build1.json
    tail -n +4 build1.json > build2.json
    rm -rf build1.json build.json
    mv build2.json build.json
    echo "," >> build.json
    tail -n +2 $DEVICE.json > $DEVICE1.json
    rm -rf $DEVICE.json
    mv $DEVICE1.json $DEVICE.json
    rm -rf latest.json
    touch latest.json
    cat info.json >> latest.json
    cat build.json >> latest.json
    cat $DEVICE.json >> latest.json 
    echo "$(<latest.json )"
    python3 -m jsonformatter
    cd ..
}  

function send_tg() {  
    cd python
    wget https://raw.githubusercontent.com/the-channel-pro/xiaomi_channel_pro/master/requirements.txt
    pip3 install -r requirements.txt
    python3 -m tg_post
  #  tg_maintainer "✅ Post creation Passed Successfully" \

#	tg_maintainer "❌ Post creation failed" \
#	" " \
#	"This could be due to following Reasons :-" \
#	"     1. The JSON maybe having some error" \
#	"     2. The format type is unsupported" \
#	tg_maintainer " CI admin (@AndroidPie9) please fix the error"

}

get_commit
get_device
get_json
send_tg